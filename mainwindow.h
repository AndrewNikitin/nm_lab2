#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <hyperbolic.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(HyperbolicEquation &eq,QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_calc_button_clicked();

    void on_next_button_clicked();

    void on_prev_button_clicked();

private:
    HyperbolicEquation curr_eq;

    const static int EXPLICIT_SCHEME = 0;
    const static int IMPLICIT_SCHEME = 1;

    Solver solver;

    double t_max;
    int x_steps, t_steps, current_step;

    Ui::MainWindow *ui;

    void PlotError();
    void PlotSolution();
};
#endif // MAINWINDOW_H
