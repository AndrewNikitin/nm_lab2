#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    HyperbolicEquation eq(
                1.0, 3.0, 1.0, 1.0, [](double x, double t) {return exp(-x) * (cos(t + M_PI_4));},
                [](double x){return sin(M_PI_4) * exp(-x);}, [](double x){return cos(M_PI_4) * exp(-x);},
                0.5, 0.25, [](double t){return -0.25 * sin(M_PI_4 + t);},
                0.25, 0.5, [](double t){return +0.25 * exp(-2) * sin(M_PI_4 + t);},
                2.0, [](double x, double t){return exp(-x) * sin(t + M_PI_4);}
            );
    QApplication a(argc, argv);
    MainWindow w(eq);
    w.show();
    return a.exec();
}
