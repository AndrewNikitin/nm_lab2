#include "hyperbolic.h"


void Solver::ImplicitScheme(const HyperbolicEquation &eq, int t_steps, double t_max, int x_steps, int boundary_approximation, int initial_approximation)
{
    u = NumericSolution(t_steps + 1, std::vector<double> (x_steps + 1));

    double h = eq.length / x_steps;
    double tau = t_max / t_steps;

    for (int j = 0; j <= x_steps; ++j)
    {
        u[0][j] = eq.phi(j * h);
    }

    if (initial_approximation == Solver::INITIAL_1_ORDER)
    {
        for (int j = 0; j <= x_steps; ++j)
        {
            u[1][j] = u[0][j] + tau * eq.psi(j * h);
        }
    }
    else if (initial_approximation == Solver::INITIAL_2_ORDER)
    {
        for (int j = 1; j < x_steps; ++j)
        {
            double u_xx = (u[0][j + 1] - 2 * u[0][j] + u[0][j - 1]) / (h * h);
            double u_x = (u[0][j + 1] - u[0][j - 1]) / (2 * h);
            double f = eq.f(h * j, 0);
            u[1][j] = u[0][j] + tau * eq.psi(j * h) + tau * tau * (eq.a * u_xx + eq.b * u_x + eq.c * u[0][j] + f - eq.d * eq.psi(j * h)) / 2;
        }
        double k00 = -eq.alpha1 * 3 / (2 * h) + eq.beta1;
        double k01 = eq.alpha1 * 4 / (2 * h);
        double k02 = -eq.alpha1 / ( 2 * h);

        u[1][0] = -k01 * u[1][1]/ k00 - k02 * u[1][2] / k00 + eq.gamma1(tau) / k00;

        double k10 = eq.alpha2 / (2 * h);
        double k11 = -eq.alpha2 * 4 / (2 * h);
        double k12 = eq.alpha2  * 3 / (2 * h) + eq.beta2;
    
        u[1][x_steps] = -k10 * u[1][x_steps - 2]/ k12 - k11 * u[1][x_steps - 1] / k12 + eq.gamma2(tau) / k12;
    }

    std::vector<double> upper(x_steps + 1, -eq.a/(h * h) - eq.b/(2*h));
    std::vector<double> diag(x_steps + 1, 1/(tau * tau) + eq.d/(2*tau) + 2*eq.a/(h * h) - eq.c);
    std::vector<double> lower(x_steps + 1, -eq.a/(h * h) + eq.b/(2*h));
    std::vector<double> rhs(x_steps + 1);

    for (int i = 1; i < t_steps; ++i)
    {
        for (int j = 1; j < x_steps; ++j)
        {
            rhs[j] = -(-2*u[i][j] + u[i - 1][j])/(tau*tau) + eq.d*u[i - 1][j]/(2*tau) + eq.f(j * h, (i + 1) * tau);
        }

        if (boundary_approximation == Solver::BOUNDARY_2_POINTS_1_ORDER)
        {
            diag[0] = - eq.alpha1 + eq.beta1 * h;
            upper[0] = eq.alpha1;
            rhs[0] = h * eq.gamma1((i + 1) * tau);

            lower[x_steps] = -eq.alpha2;
            diag[x_steps] = eq.alpha2 + eq.beta2 * h;
            rhs[x_steps] = h * eq.gamma2((i + 1) * tau);
        }
        else if (boundary_approximation == Solver::BOUNDARY_2_POINTS_2_ORDER)
        {
            double k00 = -eq.alpha1*(-2*eq.c*h*h*tau*tau + eq.d*h*h*tau + 4*eq.a*tau*tau + 2*h*h)/(2*tau*tau*h*(-eq.b*h + 2*eq.a)) + eq.beta1;
            double k01 = 2*eq.alpha1*eq.a/(h*(-eq.b*h + 2*eq.a));
            double k02 = eq.alpha1*(-eq.d*h*h*tau*u[i - 1][0] - 2*h*h*tau*tau*eq.f(0,(i+1)*tau) - 4*h*h*u[i][0] + 2*h*h*u[i - 1][0])/(2*tau*tau*h*(-eq.b*h + 2*eq.a)) 
                         + eq.gamma1(tau * (i + 1));

            diag[0] = k00;
            upper[0] = k01;
            rhs[0] = k02;

            double k10 = -2*eq.alpha2*eq.a/(h*(eq.b*h + 2*eq.a));
            double k11 = eq.alpha2*(-2*eq.c*h*h*tau*tau + eq.d*h*h*tau + 4*eq.a*tau*tau + 2*h*h)/(2*tau*tau*h*(eq.b*h + 2*eq.a)) + eq.beta2;
            double k12 = -eq.alpha2*(-eq.d*h*h*tau*u[i - 1][x_steps] - 2*h*h*tau*tau*eq.f(eq.length,(i+1)*tau) - 4*h*h*u[i][x_steps] + 2*h*h*u[i - 1][x_steps])/(2*tau*tau*h*(eq.b*h + 2*eq.a)) 
                         + eq.gamma2(tau * (i + 1));
            
            lower[x_steps] = k10;
            diag[x_steps] = k11;
            rhs[x_steps] = k12;
        }
        else if (boundary_approximation == Solver::BOUNDARY_3_POINTS_2_ORDER)
        {
            double k00 = -eq.alpha1 * 3 / (2 * h) + eq.beta1;
            double k01 = eq.alpha1 * 4 / (2 * h);
            double k02 = -eq.alpha1 / (2 * h);

            diag[0] = k00 - lower[1] * k02 / upper[1];
            upper[0] = k01 - diag[1] * k02 / upper[1];
            rhs[0] = eq.gamma1((i+1) * tau) - rhs[1] * k02 / upper[1];

            double k10 = eq.alpha2 / (2 * h);
            double k11 = -eq.alpha2 * 4 / (2 * h);
            double k12 = eq.alpha2  * 3 / (2 * h) + eq.beta2;
            
            lower[x_steps] = k11 - k10 * diag[x_steps - 1] / lower[x_steps - 1];
            diag[x_steps] = k12 - k10 * upper[x_steps - 1] / lower[x_steps - 1];
            rhs[x_steps] = eq.gamma2((i + 1) * tau) - k10 * rhs[x_steps - 1] / lower[x_steps - 1];

        }

        TridiagonalAlgorithm(lower, diag, upper, rhs, u[i + 1], x_steps + 1);
    }
}

/// Явная конечно-разностная схема решения
void Solver::ExplicitScheme(const HyperbolicEquation &eq, int t_steps, double t_max, int x_steps, int boundary_approximation, int initial_approximation)
{
    u = NumericSolution(t_steps + 1, std::vector<double> (x_steps + 1));

    double h = eq.length / x_steps;
    double tau = t_max / t_steps;

    if ( eq.a * tau >= h )
    {
        std::cerr << "WARNING: A * tau >= h\n";
    }

    for (int j = 0; j <= x_steps; ++j)
    {
        u[0][j] = eq.phi(j * h);
    }

    if (initial_approximation == Solver::INITIAL_1_ORDER)
    {
        for (int j = 0; j <= x_steps; ++j)
        {
            u[1][j] = u[0][j] + tau * eq.psi(j * h);
        }
    }
    else if (initial_approximation == Solver::INITIAL_2_ORDER)
    {
        for (int j = 1; j < x_steps; ++j)
        {
            double u_xx = (u[0][j + 1] - 2 * u[0][j] + u[0][j - 1]) / (h * h);
            double u_x = (u[0][j + 1] - u[0][j - 1]) / (2 * h);
            double f = eq.f(h * j, 0);
            u[1][j] = u[0][j] + tau * eq.psi(j * h) + tau * tau * (eq.a * u_xx + eq.b * u_x + eq.c * u[0][j] + f - eq.d * eq.psi(j * h)) / 2;
        }
        double k00 = -eq.alpha1 * 3 / (2 * h) + eq.beta1;
        double k01 = eq.alpha1 * 4 / (2 * h);
        double k02 = -eq.alpha1 / ( 2 * h);

        u[1][0] = -k01 * u[1][1]/ k00 - k02 * u[1][2] / k00 + eq.gamma1(tau) / k00;

        double k10 = eq.alpha2 / (2 * h);
        double k11 = -eq.alpha2 * 4 / (2 * h);
        double k12 = eq.alpha2  * 3 / (2 * h) + eq.beta2;
    
        u[1][x_steps] = -k10 * u[1][x_steps - 2]/ k12 - k11 * u[1][x_steps - 1] / k12 + eq.gamma2(tau) / k12;
    }

    for (int i = 1; i < t_steps; ++i)
    {
        for (int j = 1; j < x_steps; ++j)
        {
            u[i + 1][j] = -(-eq.b*h*tau*tau - 2*eq.a*tau*tau)*u[i][j + 1]/(h*h*(eq.d*tau + 2)) \
                          -(-2*eq.c*h*h*tau*tau + 4*eq.a*tau*tau - 4*h*h)*u[i][j]/(h*h*(eq.d*tau + 2)) \
                          -(eq.b*h*tau*tau - 2*eq.a*tau*tau)*u[i][j - 1]/(h*h*(eq.d*tau + 2)) \
                          -(-eq.d*h*h*tau + 2*h*h)*u[i - 1][j]/(h*h*(eq.d*tau + 2))\
                          + 2*tau*tau*eq.f(j * h, i * tau)/(eq.d*tau + 2);
        }

        if (boundary_approximation == Solver::BOUNDARY_2_POINTS_1_ORDER)
        {
            u[i + 1][0] = -(-eq.alpha1 * u[i + 1][1] + h * eq.gamma1((i + 1) * tau)) / (eq.alpha1 - eq.beta1 * h);
            u[i + 1][x_steps] = (eq.alpha2 * u[i + 1][x_steps - 1] + h * eq.gamma2((i + 1) * tau)) / (eq.alpha2 + eq.beta2 * h);
        }
        else if (boundary_approximation == Solver::BOUNDARY_2_POINTS_2_ORDER)
        {
            double k00 = -eq.alpha1*(-2*eq.c*h*h*tau*tau + eq.d*h*h*tau + 4*eq.a*tau*tau + 2*h*h)/(2*tau*tau*h*(-eq.b*h + 2*eq.a)) + eq.beta1;
            double k01 = 2*eq.alpha1*eq.a/(h*(-eq.b*h + 2*eq.a));
            double k02 = eq.alpha1*(-eq.d*h*h*tau*u[i - 1][0] - 2*h*h*tau*tau*eq.f(0,(i+1)*tau) - 4*h*h*u[i][0] + 2*h*h*u[i - 1][0])/(2*tau*tau*h*(-eq.b*h + 2*eq.a)) 
                         + eq.gamma1(tau * (i + 1));

            u[i + 1][0] = -k01 * u[i + 1][1] / k00 + k02 / k00;

            double k10 = -2*eq.alpha2*eq.a/(h*(eq.b*h + 2*eq.a));
            double k11 = eq.alpha2*(-2*eq.c*h*h*tau*tau + eq.d*h*h*tau + 4*eq.a*tau*tau + 2*h*h)/(2*tau*tau*h*(eq.b*h + 2*eq.a)) + eq.beta2;
            double k12 = -eq.alpha2*(-eq.d*h*h*tau*u[i - 1][x_steps] - 2*h*h*tau*tau*eq.f(eq.length,(i+1)*tau) - 4*h*h*u[i][x_steps] + 2*h*h*u[i - 1][x_steps])/(2*tau*tau*h*(eq.b*h + 2*eq.a)) 
                         + eq.gamma2(tau * (i + 1));
            
            u[i + 1][x_steps] = -k10 * u[i + 1][x_steps - 1] / k11 + k12 / k11;
        }
        else if (boundary_approximation == Solver::BOUNDARY_3_POINTS_2_ORDER)
        {
            double k00 = -eq.alpha1 * 3 / (2 * h) + eq.beta1;
            double k01 = eq.alpha1 * 4 / (2 * h);
            double k02 = -eq.alpha1 / (2 * h);

            u[i + 1][0] = -k01 * u[i + 1][1]/ k00 - k02 * u[i + 1][2] / k00 + eq.gamma1(tau * (i + 1)) / k00;

            double k10 = eq.alpha2 / (2 * h);
            double k11 = -eq.alpha2 * 4 / (2 * h);
            double k12 = eq.alpha2  * 3 / (2 * h) + eq.beta2;
            
            u[i + 1][x_steps] = -k10 * u[i + 1][x_steps - 2]/ k12 - k11 * u[i + 1][x_steps - 1] / k12 + eq.gamma2(tau * (i + 1)) / k12;
        }
    }
}
