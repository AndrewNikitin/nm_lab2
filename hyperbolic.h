#ifndef HYPERBOLIC_H
#define HYPERBOLIC_H

#include <iostream>
#include <functional>
#include <vector>
#include <cmath>

#include "tridiagonal.h"


using NumericSolution = std::vector<std::vector<double>>;

class HyperbolicEquation
{
public:
    double a, b, c, d;

    std::function<double(double, double)> f;
    std::function<double(double)> phi;
    std::function<double(double)> psi;

    double alpha1;
    double alpha2;

    double beta1;
    double beta2;

    std::function<double(double)> gamma1;
    std::function<double(double)> gamma2;

    std::function<double(double, double)> solution;

    double length;

    HyperbolicEquation(double a, double b, double c, double d, std::function<double(double, double)> f, 
                       std::function<double(double)> phi, std::function<double(double)> psi,
                       double alpha1, double beta1, std::function<double(double)> gamma1,
                       double alpha2, double beta2, std::function<double(double)> gamma2, 
                       double length, std::function<double(double, double)> solution
                     ) 
    {
        this->a = a;
        this->b = b;
        this->c = c;
        this->d = d;
        this->f = f;

        this->phi = phi;
        this->psi = psi;

        this->alpha1 = alpha1;
        this->beta1 = beta1;
        this->gamma1 = gamma1;
         
        this->alpha2 = alpha2;
        this->beta2 = beta2;
        this->gamma2 = gamma2;

        this->length = length;

        this->solution = solution;
    }
};

class Solver
{
public:
    const static int BOUNDARY_2_POINTS_1_ORDER = 0;
    const static int BOUNDARY_2_POINTS_2_ORDER = 1;
    const static int BOUNDARY_3_POINTS_2_ORDER = 2;

    const static int INITIAL_1_ORDER = 0;
    const static int INITIAL_2_ORDER = 1;

    void ImplicitScheme(const HyperbolicEquation &eq, int t_steps, double t_max, int x_steps, int boundary_approximation, int initial_approximation);
    void ExplicitScheme(const HyperbolicEquation &eq, int t_steps, double t_max, int x_steps, int boundary_approximation, int initial_approximation);
    
    const NumericSolution& GetSolution() 
    {
        return u;
    }

    ~Solver() {}

private:
    NumericSolution u;
};

#endif
