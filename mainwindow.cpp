#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(HyperbolicEquation &eq, QWidget *parent)
    : QMainWindow(parent)
    , curr_eq(eq)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowTitle("ЛР №1");

    ui->tsteps_spinbox->setRange(0, 1000000);

    ui->a_spinbox->setValue(eq.a);
    ui->b_spinbox->setValue(eq.b);
    ui->c_spinbox->setValue(eq.c);
    ui->d_spinbox->setValue(eq.d);

    ui->f_lineedit->setText("e^(-x)*(cos(t+pi/4)-sin(t+pi/4))");
    ui->phi_lineedit->setText("sin(pi/4) * e^(-x)");
    ui->psi_lineedit->setText("cos(pi/4) * e^(-x)");

    ui->alpha1_spinbox->setValue(eq.alpha1);
    ui->beta1_spinbox->setValue(eq.beta1);
    ui->gamma1_lineedit->setText("-0.75*sin(pi/4+t)");

    ui->alpha2_spinbox->setValue(eq.alpha2);
    ui->beta2_spinbox->setValue(eq.beta2);
    ui->gamma2_lineedit->setText("+2.90*e^(-2)*sin(pi/4+t)");

    ui->xmax_spinbox->setValue(eq.length);

    ui->solution_plot->addGraph(ui->solution_plot->xAxis, ui->solution_plot->yAxis);
    ui->solution_plot->addGraph(ui->solution_plot->xAxis, ui->solution_plot->yAxis);
    ui->solution_plot->graph(0)->setPen(QPen(QColor(0, 0, 255), 2));
    ui->solution_plot->graph(1)->setPen(QPen(QColor(255, 0, 0), 2));
    ui->solution_plot->xAxis->setLabel("x");
    ui->solution_plot->yAxis->setLabel("U(x, t)");

    ui->error_plot->addGraph(ui->error_plot->xAxis, ui->error_plot->yAxis);
    ui->error_plot->graph(0)->setPen(QPen(QColor(0, 0, 255), 2));
    ui->error_plot->xAxis->setLabel("t");
    ui->error_plot->yAxis->setLabel("Error(t)");

    t_max = 0.0;
    x_steps = t_steps = current_step = -1;
}



MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_calc_button_clicked()
{
    current_step = 0;
    t_max = ui->tmax_spinbox->value();
    x_steps = ui->xsteps_spinbox->value();
    t_steps = ui->tsteps_spinbox->value();

    int method_index = ui->methd_cbox->currentIndex();
    int init_cond_index = ui->init_cond_cbox->currentIndex();
    int boundary_cond_index = ui->boundary_cond_cbox->currentIndex();

    if (method_index == EXPLICIT_SCHEME)
    {
        if ( curr_eq.a * t_max / t_steps >= curr_eq.length / x_steps)
        {
            x_steps = int(curr_eq.length * t_steps / (curr_eq.a * t_max));
            ui->xsteps_spinbox->setValue(x_steps);
        }
        solver.ExplicitScheme(curr_eq, t_steps, t_max, x_steps, boundary_cond_index, init_cond_index);
    }
    else if (method_index == IMPLICIT_SCHEME)
    {
        solver.ImplicitScheme(curr_eq, t_steps, t_max, x_steps, boundary_cond_index, init_cond_index);
    }

    PlotError();
    PlotSolution();
}

void MainWindow::PlotError()
{
    double tau = t_max / t_steps;
    double h = curr_eq.length / x_steps;
    QVector<double> error_by_t(t_steps + 1, 0);
    QVector<double> t(t_steps + 1);
    for (int i = 0; i <= t_steps; ++i)
    {
        t[i] = tau * i;
        for (int j = 0; j <= x_steps; ++j)
        {
            error_by_t[i] = std::max(std::abs(curr_eq.solution(j*h, i*tau) - solver.GetSolution()[i][j]), error_by_t[i]);
        }
    }
    ui->error_plot->graph(0)->setData(t, error_by_t);
    ui->error_plot->rescaleAxes();
    ui->error_plot->replot();
}

void MainWindow::PlotSolution()
{
    QVector<double> u(x_steps + 1, 0);
    QVector<double> x(x_steps + 1, 0);

    for (int i = 0; i <= x_steps; ++i)
    {
        x[i] = i * (curr_eq.length / x_steps);
        u[i] = solver.GetSolution()[current_step][i];
    }
    ui->solution_plot->graph(1)->setData(x, u);

    double t = current_step * t_max / t_steps;
    for (int i = 0; i <= x_steps; ++i)
    {
        u[i] = curr_eq.solution(i*(curr_eq.length/x_steps), t);
    }
    ui->solution_plot->graph(0)->setData(x, u);

    ui->solution_plot->rescaleAxes();
    ui->solution_plot->replot();
}

void MainWindow::on_next_button_clicked()
{
    if (current_step == -1)
        return;
    current_step = (current_step + 1) % (t_steps + 1);
    PlotSolution();
}

void MainWindow::on_prev_button_clicked()
{
    if (current_step == -1)
        return;
    current_step = (current_step + t_steps) % (t_steps + 1);
    PlotSolution();
}
