#ifndef TRIDIAGONAL_H
#define TRIDIAGONAL_H

#include <vector>
#include <iostream>
using VecDouble = std::vector<double>;

void TridiagonalAlgorithm(VecDouble &lower, VecDouble &diag, VecDouble &upper, VecDouble &rhs, VecDouble &ans, int n);

#endif
